import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { join } from 'path';
import { LightsaberModule } from './lightsaber/lightsaber.module';
import { USERNAME, PW, DB } from './secrets';
@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: '/cloudsql/star-wars-265314:europe-west6:star-wars',
      port: 5432,
      username: USERNAME,
      password: PW,
      database: DB,
      entities: [join(__dirname, '**/**.entity{.ts,.js}')],
      synchronize: true,
    }),
    LightsaberModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
