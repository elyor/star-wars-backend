
export interface Lightsaber {
    id: number;
    name: string;
    color: string;
    length: number;
}
