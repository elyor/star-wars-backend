import { Module } from '@nestjs/common';
import { LightsaberController } from './lightsaber.controller';
import { LightsaberService } from './lightsaber.service';
import { Lightsaber } from './lightsaber.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  controllers: [LightsaberController],
  providers: [LightsaberService],
  imports: [
    TypeOrmModule.forFeature([Lightsaber]),
  ],
})
export class LightsaberModule { }
