import { Injectable } from '@nestjs/common';
import { Lightsaber } from './lightsaber.entity';
import { Repository, DeleteResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class LightsaberService {
    constructor(
        @InjectRepository(Lightsaber)
        private lightsaberRepository: Repository<Lightsaber>,
    ) { }
    async getAll(): Promise<Lightsaber[]> {
        return await this.lightsaberRepository.find();
    }
    async add(lightsaber: Lightsaber): Promise<Lightsaber> {
        const newLightsaber = await this.lightsaberRepository.create(lightsaber);
        return await this.lightsaberRepository.save(newLightsaber);
    }
    async get(id: string): Promise<Lightsaber> {
        return await this.lightsaberRepository.findOne(id);
    }
    async update(id: string, lightsaber: Partial<Lightsaber>): Promise<Lightsaber> {
        await this.lightsaberRepository.update(id, lightsaber);
        return await this.lightsaberRepository.findOne(id);
    }
    async remove(id: string): Promise<DeleteResult> {
        await this.lightsaberRepository.findOneOrFail(id);
        return await this.lightsaberRepository.delete(id);
    }
}
